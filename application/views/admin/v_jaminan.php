<div class="row">
    <div class="container-fluid">
        <div class="box">
            <div class="box-header"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah <span class="fa fa-plus"></span></a></div>
            <div class="box-body">
                <table id="jenis_tbl" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Jaminan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($jaminan as $key) {
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $key->nama_jaminan ?></td>
                                <td><a href="#" data-toggle="modal" data-target="#ModalEdit<?php echo $key->id_jaminan ?>" class="btn btn-info">Edit</a> <a href="<?php echo base_url('admin/jaminan/hapus/'.$key->id_jaminan) ?>" class="btn btn-danger">Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Jaminan</h4>
      </div>
      <form action="<?php echo base_url('admin/jaminan/tambah_jaminan') ?>" class="form-horizontal" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Nama Jaminan</label>
                <div class="col-sm-9">
                    <input type="text" name="nama_jaminan" class="form-control" placeholder="Nama Jaminan">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
foreach ($jaminan as $keyEdit) {
    ?>
    <div class="modal fade" id="ModalEdit<?php echo $keyEdit->id_jaminan ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data Jaminan</h4>
            </div>
            <form action="<?php echo base_url('admin/jaminan/ubah_jaminan') ?>" class="form-horizontal" method="post">
                <input type="hidden" name="id_jaminan" value="<?php echo $keyEdit->id_jaminan ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nama Jaminan</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_jaminan" class="form-control" placeholder="Nama Jaminan" value="<?php echo $keyEdit->nama_jaminan ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <?php
}
?>