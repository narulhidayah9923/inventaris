<div class="row">
    <div class="container-fluid">
        <div class="box">
        <div class="box-header"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah <span class="fa fa-plus"></span></a></div>
            <div class="box-body">
                <table id="jenis_tbl" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <!-- <th>Password</th> -->
                            <th>Nama User</th>
                            <th>Level</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php 
                        $no = 1;
                        foreach ($user as $key) {
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $key->username ?></td>
                                <!-- <td><?php echo $key->password ?></td> -->
                                <td><?php echo $key->nama_user ?></td>
                                <td><?php echo $key->nama_level ?></td>
                                <td><a href="#" data-toggle="modal" data-target="#ModalEdit<?php echo $key->id_user ?>" class="btn btn-info">Edit</a> <a href="<?php echo base_url('admin/user/hapus/'.$key->id_user) ?>" class="btn btn-danger">Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data User</h4>
      </div>
      <form action="<?php echo base_url('admin/user/tambah_user') ?>" class="form-horizontal" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Username</label>
                <div class="col-sm-9">
                    <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Nama User</label>
                <div class="col-sm-9">
                    <input type="text" name="nama_user" class="form-control" placeholder="Nama User" required>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Level</label>
                <div class="col-sm-9">
                    <select name="id_level" class="form-control">
                        <?php
                        foreach ($level as $key_lvl) {
                            ?>
                            <option value="<?php echo $key_lvl->id_level ?>"><?php echo $key_lvl->nama_level ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
foreach ($user as $keyEdit) {
    ?>
    <div class="modal fade" id="ModalEdit<?php echo $keyEdit->id_user ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data User</h4>
            </div>
            <form action="<?php echo base_url('admin/user/ubah_user') ?>" class="form-horizontal" method="post">
                <input type="hidden" name="id_user" value="<?php echo $keyEdit->id_user ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $keyEdit->username ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $keyEdit->password ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nama User</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_user" class="form-control" placeholder="Nama User" value="<?php echo $keyEdit->nama_user ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Level</label>
                        <div class="col-sm-9">
                            <select name="id_level" class="form-control">
                                <?php
                                foreach ($level as $key_lvl) {
                                    ?>
                                    <option value="<?php echo $key_lvl->id_level ?>" <?php if($key_lvl->id_level == $keyEdit->id_level){ echo "selected"; }  ?>><?php echo $key_lvl->nama_level ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <?php
}
?>