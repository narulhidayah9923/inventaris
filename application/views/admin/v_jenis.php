<div class="row">
    <div class="container-fluid">
        <div class="box">
            <div class="box-header"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah <span class="fa fa-plus"></span></a></div>
            <div class="box-body">
                <table id="jenis_tbl" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode jenis</th>
                            <th>Nama jenis</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($jenis as $key) {
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $key->kode_jenis ?></td>
                                <td><?php echo $key->nama_jenis ?></td>
                                <td><?php echo $key->keterangan ?></td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#ModalEdit<?php echo $key->id_jenis ?>" class="btn btn-info">Edit</a> 
                                    <a href="<?php echo base_url('admin/jenis/hapus/'.$key->id_jenis) ?>" class="btn btn-danger">Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Jenis</h4>
      </div>
      <form action="<?php echo base_url('admin/jenis/tambah_jenis') ?>" class="form-horizontal" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kode Jenis</label>
                <div class="col-sm-9">
                    <input type="text" name="kode_jenis" class="form-control" placeholder="Kode Jenis" value="KJ<?php echo  date('hms'). rand(0000,9999) ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Nama Jenis</label>
                <div class="col-sm-9">
                    <input type="text" name="nama_jenis" class="form-control" placeholder="Nama Jenis">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Keterangan</label>
                <div class="col-sm-9">
                    <input type="text" name="keterangan" class="form-control" placeholder="Keterangan">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
foreach ($jenis as $keyEdit) {
    ?>
    <div class="modal fade" id="ModalEdit<?php echo $keyEdit->id_jenis ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Jenis</h4>
            </div>
            <form action="<?php echo base_url('admin/jenis/ubah_jenis') ?>" class="form-horizontal" method="post">
                <input type="hidden" name="id_jenis" value="<?php echo $keyEdit->id_jenis ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Kode Jenis</label>
                        <div class="col-sm-9">
                            <input type="text" name="kode_jenis" class="form-control" placeholder="Kode Jenis" value="<?php echo $keyEdit->kode_jenis ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nama Jenis</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_jenis" class="form-control" placeholder="Nama Jenis" value="<?php echo $keyEdit->nama_jenis ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" value="<?php echo $keyEdit->keterangan ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <?php
}
?>