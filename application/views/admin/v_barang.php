<div class="row">
    <div class="container-fluid">
        <div class="box">
        <div class="box-header"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah <span class="fa fa-plus"></span></a></div>
            <div class="box-body">
                <?php 
                if($this->session->flashdata('error')){
                    ?>
                    <div class="card-danger">
                        <?php echo $this->session->flashdata('error') ?>
                    </div>
                    <?php
                }
                ?>
                <table id="jenis_tbl" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Stok</th>
                            <th>Jenis</th>
                            <th>Ruang</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($barang as $key) {
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $key->kode_barang ?></td>
                                <td><?php echo $key->nama_barang ?></td>
                                <td><?php echo $key->jumlah_barang ?></td>
                                <td><?php echo $key->nama_jenis ?></td>
                                <td><?php echo $key->nama_ruang ?></td>
                                <td><a href="#" data-toggle="modal" data-target="#ModalEdit<?php echo $key->id_barang ?>" class="btn btn-info">Edit</a> <a href="<?php echo base_url('admin/barang/hapus/'.$key->id_barang) ?>" class="btn btn-danger">Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Jenis</h4>
      </div>
      <form action="<?php echo base_url('admin/barang/tambah_barang') ?>" class="form-horizontal" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kode Barang</label>
                <div class="col-sm-9">
                    <input type="text" name="kode_barang" class="form-control" placeholder="Kode Barang" value="KB<?php echo  date('hms'). rand(0000,9999) ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Nama Barang</label>
                <div class="col-sm-9">
                    <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Keterangan</label>
                <div class="col-sm-9">
                    <input type="text" name="keterangan" class="form-control" placeholder="Keterangan">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jumlah Barang</label>
                <div class="col-sm-9">
                    <input type="number" name="jumlah_barang" class="form-control" placeholder="Jumlah Barang">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jenis</label>
                <div class="col-sm-9">
                    <select name="id_jenis" class="form-control">
                        <?php
                        foreach ($jenis as $key_jns) {
                            ?>
                            <option value="<?php echo $key_jns->id_jenis ?>"><?php echo $key_jns->nama_jenis ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Ruang</label>
                <div class="col-sm-9">
                    <select name="id_ruang" class="form-control">
                        <?php
                        foreach ($ruang as $key_rng) {
                            ?>
                            <option value="<?php echo $key_rng->id_ruang ?>"><?php echo $key_rng->nama_ruang ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
foreach ($barang as $keyBarang) {
    ?>
    <div class="modal fade" id="ModalEdit<?php echo $keyBarang->id_barang ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Jenis</h4>
            </div>
            <form action="<?php echo base_url('admin/barang/ubah_barang') ?>" class="form-horizontal" method="post">
                <input type="hidden" name="id_barang" value="<?php echo $keyBarang->id_barang ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Kode Barang</label>
                        <div class="col-sm-9">
                            <input type="text" name="kode_barang" class="form-control" placeholder="Kode Barang" value="<?php echo $keyBarang->kode_barang ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nama Barang</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" value="<?php echo $keyBarang->nama_barang ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" value="<?php echo $keyBarang->keterangan ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Jumlah Barang</label>
                        <div class="col-sm-9">
                            <input type="number" name="jumlah_barang" class="form-control" placeholder="Jumlah Barang" value="<?php echo $keyBarang->jumlah_barang ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Jenis</label>
                        <div class="col-sm-9">
                            <select name="id_jenis" class="form-control">
                                <?php
                                foreach ($jenis as $key_jns) {
                                    ?>
                                    <option value="<?php echo $key_jns->id_jenis ?>" <?php if($key_jns->id_jenis == $keyBarang->id_jenis){ echo "selected"; } ?>><?php echo $key_jns->nama_jenis ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Ruang</label>
                        <div class="col-sm-9">
                            <select name="id_ruang" class="form-control">
                                <?php
                                foreach ($ruang as $key_rng) {
                                    ?>
                                    <option value="<?php echo $key_rng->id_ruang ?>" <?php if($key_rng->id_ruang == $keyBarang->id_ruang){ echo "selected"; } ?>><?php echo $key_rng->nama_ruang ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
        </div>
    <?php
}
?>