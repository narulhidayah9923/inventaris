<div class="row">
    <div class="container-fluid">
        <div class="box">
        	<?php
        	if($pesan == ''){

        	}else {
        		?><br/>
        		<div class="container-fluid">
	        		<div class="box box-danger">
	        			<div class="box-body">
	        				<?php echo $pesan ?>
	        			</div>
	        		</div>
        		</div>
        		<?php
        	}
        	?>
            <div class="box-header">
                <!-- <h4>Peminjaman</h4><hr/> -->
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah <span class="fa fa-plus"></span></a>
                <?php if($this->session->userdata('level') == 'admin'){ ?> <a href="<?php echo base_url('admin/laporan') ?>" target="a_blank" class="btn btn-info"><span class="fa fa-print"></span>Cetak Laporan</a> <?php }?>
            </div>
            <div class="box-body">
                <table id="peminjaman_tbl" class="table">
                    <thead>
                        <tr>
                        	<th style="width:10px">No</th>
                        	<th>Kode Peminjaman</th>
                        	<th>Tanggal Peminjaman</th>
                        	<th>Nama Barang</th>
                        	<th style="width:10px">Jumlah</th>
                        	<th>User</th>
                        	<th>Tanggal Kembali</th>
                        	<th>Jaminan</th>
                        	<th>Keperluan</th>
                        	<th>Status</th>
                            <?php if($this->session->userdata('level') != 'peminjam'){ ?>
                        	<th>Aksi</th>
                            <?php } ?>
                        </tr>
                    </thead>
                   	<tbody>
                   		<?php 
                   		$no = 1;
                   		foreach ($peminjaman as $key) {
                   			?>
                   			<tr>
                   				<td><?php echo $no++ ?></td>
                   				<td><?php echo $key->kode_peminjaman ?></td>
                   				<td><?php echo $key->tgl_peminjaman ?></td>
                   				<td><?php echo $key->nama_barang ?></td>
                   				<td><?php echo $key->jumlah_pinjam ?></td>
                   				<td><?php echo $key->nama_user ?></td>
                   				<td><?php echo $key->tgl_kembali_peminjaman ?></td>
                   				<td><?php echo $key->nama_jaminan ?></td>
                   				<td><?php echo $key->keperluan ?></td>
                   				<td><?php if($key->status == "sudah dikembalikan"){ echo "<p style='color:green'>" . $key->status . "</p>" ;  } else { echo "<p style='color:red'>" . $key->status ."</p>"; }  ?></td>
                                <?php if($this->session->userdata('level') != 'peminjam'){ ?>
                   				<td> <?php if($key->status == 'belum dikembalikan'){ ?> <a href="<?php if($key->status == 'sudah dikembalikan'){ echo "#";}else{ echo base_url('admin/peminjaman/kembalikan/') . $key->id_peminjaman; } ?>" class="btn btn-xs btn-primary" <?php if($key->status == 'sudah dikembalikan'){echo "disabled";} ?>><span class="fa fa-check"></span></a>
                                    <?php }else{ echo ""; } ?>
                                </td>
                                <?php  } ?>
                   			</tr>
                   			<?php
                   		}
                   		?>
                   	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Peminjaman</h4>
      </div>
      <form action="<?php echo base_url('admin/peminjaman/tambah_peminjaman') ?>" class="form-horizontal" method="post">
        <div class="modal-body">
        	<div class="pesan">
        		<p id="test"></p>
        	</div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kode Peminjaman</label>
                <div class="col-sm-9">
                    <input type="text" name="kode_peminjaman" class="form-control" placeholder="Kode Peminjaman" value="KP<?php echo  date('hms'). rand(0000,9999) ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Tanggal Peminjaman</label>
                <div class="col-sm-9">
                    <input type="date" name="tgl_peminjaman" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Barang</label>
                <div class="col-sm-9">
                    <select name="id_barang" id="id_barang" class="form-control">
                        <?php
                        foreach ($barang as $key_brng) {
                            ?>
                            <option stock="<?php echo $key_brng->jumlah_barang ?>" value="<?php echo $key_brng->id_barang ?>"><?php echo $key_brng->nama_barang ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jumlah Pinjam</label>
                <div class="col-sm-9">

                    <input type="number" name="jumlah_pinjam" class="form-control" id="pinjam_int" placeholder="Jumlah Pinjam">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">User</label>
                <div class="col-sm-9">
                    <select name="id_user" class="form-control">
                        <?php
                        foreach ($user as $key_user) {
                            ?>
                            <option value="<?php echo $key_user->id_user ?>"><?php echo $key_user->nama_user ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Tanggal Kembali</label>
                <div class="col-sm-9">
                    <input type="date" name="tgl_kembali_peminjaman" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jaminan</label>
                <div class="col-sm-9">
                    <select name="id_jaminan" class="form-control">
                        <?php
                        foreach ($jaminan as $key_jmnn) {
                            ?>
                            <option value="<?php echo $key_jmnn->id_jaminan ?>"><?php echo $key_jmnn->nama_jaminan ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Keperluan</label>
                <div class="col-sm-9">
                    <input type="text" name="keperluan" class="form-control">
                </div>
            </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" id="btn-save" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>