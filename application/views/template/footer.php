<?php $konfigurasi = $this->db->query("SELECT * from konfigurasi_aplikasi")->row(); ?>
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="<?php echo $konfigurasi->url_web; ?>"><?php echo $konfigurasi->title_name ?></a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dist/js/adminlte.min.js"></script>
<!-- <script src="<?php echo base_url() ?>assets/bower_components/DataTables-1.10.16/js/dataTables.jqueryui.min.js"></script> -->
<!-- Sparkline -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/bower_components/Chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url() ?>assets/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#ruang_tbl').DataTable();
    $('#jenis_tbl').DataTable();
    $('#peminjaman_tbl').DataTable({
      scrollY: 400,
    });
    // $('#pinjam_int').change(function(){
    //   // alert('test');
    //   stock = $('#id_barang option:selected').attr('stock');
    //   jumlah_pinjam = $('#pinjam_int').val();
    //   if(stock < jumlah_pinjam){
    //     $('#test').val('testt');
    //     alert('Stock tidak cukup harap dikurangi');
    //   }else{

    //   }
    // });
    /*$('#username').change(function(){
      alert('test');
    });*/
  });

</script>
</body>
</html>
