<?php
class M_konfigurasi extends CI_Model{

    function get(){
        $this->db->select('*');
        $this->db->from('konfigurasi_aplikasi');
        return $this->db->get();
    }

    function update($data){
        $this->db->where('id', 1);
        $this->db->update('konfigurasi_aplikasi', $data);
    }

}