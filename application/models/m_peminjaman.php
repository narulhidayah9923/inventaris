<?php
class M_peminjaman extends CI_Model{

    function read(){
        $this->db->select('*');
        $this->db->from('in_peminjaman a');
        $this->db->join('in_barang b','b.id_barang = a.id_barang');
        $this->db->join('in_user c','c.id_user = a.id_user');
        $this->db->join('in_jaminan d','d.id_jaminan = a.id_jaminan');
        return $this->db->get();
    }

}