<?php
class M_barang extends CI_Model{

    function read(){
        $this->db->select('*');
        $this->db->from('in_barang');
        $this->db->join('in_ruang','in_ruang.id_ruang = in_barang.id_ruang');
        $this->db->join('in_jenis','in_jenis.id_jenis = in_barang.id_jenis');
        return $this->db->get();
    }

}