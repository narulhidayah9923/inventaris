<?php
class M_web extends CI_Model{

    function read($table){
        $this->db->select('*');
        $this->db->from($table);
        return $this->db->get();
    }

    function save($table, $data){
        $this->db->insert($table, $data);
    }

    function update($where, $table, $data){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_id($table, $where){
        return $this->db->get_where($table, $where);
    }

    function test($id_user, $cek_where){
        return $this->db->query("SELECT * from in_user where id_user != '$id_user' and username = '$cek_where' ");
    }

}