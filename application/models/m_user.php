<?php
class M_user extends CI_Model{

    function read(){
        $this->db->select('*');
        $this->db->from('in_user');
        $this->db->join('in_level', 'in_level.id_level = in_user.id_level');
        return $this->db->get();
    }

    function cek_login($where){
        return $this->db->get_where('in_user', $where);
    }

}