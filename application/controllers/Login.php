<?php
class Login extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_user');
    }

    public function index(){
        $data['pesan'] = '';
        $this->load->view('v_login', $data);
    }

    public function cek_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
            'username' => $username,
            'password' => md5($password)
        );

        $cek = $this->m_user->cek_login($where);
        // var_dump($where);exit;
        if($cek->num_rows() > 0){
            $data = $cek->row();
            if($data->id_level == 1){
                $session = array(
                    'id_user' => $data->id_user,
                    'nama_user' => $data->nama_user,
                    'stts' => 'login',
                    'level' => 'admin', 
                );
                $this->session->set_userdata($session);
                redirect(base_url('index.php/admin/dashboard'));
            }elseif($data->id_level == 2){
                $session = array(
                    'id_user' => $data->id_user,
                    'nama_user' => $data->nama_user,
                    'stts' => 'login',
                    'level' => 'petugas', 
                );
                $this->session->set_userdata($session);
                redirect(base_url('index.php/petugas'));
            }elseif($data->id_level == 3){
                $session = array(
                    'id_user' => $data->id_user,
                    'nama_user' => $data->nama_user,
                    'stts' => 'login',
                    'level' => 'peminjam', 
                );
                $this->session->set_userdata($session);
                redirect(base_url('index.php/peminjam'));
            } else{
                echo "<alert>Username Atau Password salah!!</alert>";
                $this->load->view('v_login');
            }
        } 
        else{
            $data['pesan'] = "
                <div class='box box-danger'>
                    <div class='box-body'>
                        Username atau password salah
                    </div>
                </div>
                                ";
            $this->load->view('v_login', $data);
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}