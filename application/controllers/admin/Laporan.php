<?php
class Laporan extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('pdf');
		$this->load->model('m_peminjaman');
	}

	public function index(){
		$pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
		// mencetak string 
		// (lebar, tingg, teks)
        $pdf->Cell(280,7,'SMK CITRA NEGARA',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(280,7,'LAPORAN INVENTARIS BARANG SMK CITRA NEGARA',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(38,6,'KODE PEMINJAMAN',1,0);
        $pdf->Cell(25,6,'TGL PINJAM',1,0);
        $pdf->Cell(38,6,'NAMA BARANG',1,0);
		$pdf->Cell(25,6,'JMLH PINJAM',1,0);
		$pdf->Cell(25,6,'NAMA USER',1,0);
		$pdf->Cell(38,6,'TGL PENGEMBALIAN',1,0);
		$pdf->Cell(27,6,'JAMINAN',1,0);
		$pdf->Cell(35,6,'KEPERLUAN',1,0);
		$pdf->Cell(30,6,'STATUS',1,1);
        $pdf->SetFont('Arial','',9);
        $laporan = $this->m_peminjaman->read()->result();
        foreach ($laporan as $row){
			
            $pdf->Cell(38,6,$row->kode_peminjaman,1,0);
        	$pdf->Cell(25,6,$row->tgl_peminjaman,1,0);
        	$pdf->Cell(38,6,$row->nama_barang,1,0);
			$pdf->Cell(25,6,$row->jumlah_pinjam,1,0);
			$pdf->Cell(25,6,$row->nama_user,1,0);
			$pdf->Cell(38,6,$row->tgl_kembali_peminjaman,1,0);
			$pdf->Cell(27,6,$row->nama_jaminan,1,0);
			$pdf->Cell(35,6,$row->keperluan,1,0);
			$pdf->Cell(30,6,$row->status,1,1);
        }
        $pdf->Output();
	}

}