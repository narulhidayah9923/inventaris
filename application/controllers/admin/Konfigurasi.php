<?php
class Konfigurasi extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_konfigurasi');
    }

    public function index(){
        $data['konfigurasi'] = $this->m_konfigurasi->get()->row();
        $this->load->view('template/header');
        $this->load->view('admin/v_konfigurasi', $data);
        $this->load->view('template/footer');
    }

    public function update(){
        if($this->inpupt->post()){
            $title_name = $this->input->post('title_name');
            $url_web = $this->input->post('url_web');

            $data = array(
                'title_name' => $title_name,
                'url_web' => $url_web,
            );
            
            $this->m_konfigurasi->update($data);
        }else{
            redirect(base_url('konfigurasi'));
        }
    }

}