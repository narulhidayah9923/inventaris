<?php
class User extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_web');
        $this->load->model('m_user');
    }

    public function index(){
        $data['user'] = $this->m_user->read()->result();
        $data['level'] = $this->m_web->read('in_level')->result();
        $title['title'] = 'Data User';
        $this->load->view('template/header',$title);
        $this->load->view('admin/v_user',$data);
        $this->load->view('template/footer');
    }

    public function tambah_user(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $nama_user = $this->input->post('nama_user');
        $id_level = $this->input->post('id_level');

        $data = array(
            'username' => $username,
            'password' => md5($password),
            'nama_user' => $nama_user,
            'id_level' => $id_level
        );
        $cek_user = array('username' => $username);
        $user = $this->m_web->get_id('in_user',$cek_user)->num_rows();
        if($user > 0){
            echo "<script>alert('Username ini sudah digunakan coba user yang lain');</script>";
            echo "<script>location.href = '../../admin/user';</script>";
        } else{
            $this->m_web->save('in_user', $data);
            redirect(base_url('admin/user'));
        }
    }

    public function ubah_user(){
        $id_user = $this->input->post('id_user');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $nama_user = $this->input->post('nama_user');
        $id_level = $this->input->post('id_level');

        $data = array(
            'username' => $username,
            'password' => md5($password),
            'nama_user' => $nama_user,
            'id_level' => $id_level
        );
        $where = array('id_user' =>$id_user);

        $cek_user = array('username' => $username);
        $user = $this->m_web->test($id_user,$username)->num_rows();
        // var_dump($user);exit;
        if($user > 0){
            echo "<script>alert('Username ini sudah digunakan coba user yang lain');</script>";
            echo "<script>location.href = '../../admin/user';</script>";
        }else{
            $this->m_web->update($where, 'in_user', $data);
            redirect(base_url('admin/user'));
        }

        
    }
    
    public function hapus($id_user){
        $where = array('id_user' => $id_user);
        
        $this->m_web->delete($where, 'in_user');
        redirect(base_url('admin/user'));
    }

}