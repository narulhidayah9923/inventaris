<?php
class Barang extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_barang');
        $this->load->model('m_web');
    }

    public function index(){
        $data['barang']  = $this->m_barang->read()->result();
        $data['ruang'] = $this->m_web->read('in_ruang')->result();
        $data['jenis'] = $this->m_web->read('in_jenis')->result();
        $title['title'] = "Data Barang";
        $this->load->view('template/header', $title);
        $this->load->view('admin/v_barang', $data);
        $this->load->view('template/footer');
    }

    public function tambah_barang(){
        $kode_barang = $this->input->post('kode_barang');
        $nama_barang = $this->input->post('nama_barang');
        $keterangan = $this->input->post('keterangan');
        $jumlah_barang = $this->input->post('jumlah_barang');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $id_user = $this->input->post('id_user');

        $data = array(
            'kode_barang' => $kode_barang ,
            'nama_barang' => $nama_barang,
            'keterangan' => $keterangan,
            'jumlah_barang' => $jumlah_barang,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'id_user' => $id_user 
        );

        $this->m_web->save('in_barang', $data);
        redirect(base_url('admin/barang'));
    }

    public function ubah_barang(){
        $id_barang = $this->input->post('id_barang');
        $kode_barang = $this->input->post('kode_barang');
        $nama_barang = $this->input->post('nama_barang');
        $keterangan = $this->input->post('keterangan');
        $jumlah_barang = $this->input->post('jumlah_barang');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $id_user = $this->input->post('id_user');

        $data = array(
            'kode_barang' => $kode_barang ,
            'nama_barang' => $nama_barang,
            'keterangan' => $keterangan,
            'jumlah_barang' => $jumlah_barang,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'id_user' => $id_user 
        );

        $where = array('id_barang' => $id_barang);

        $this->m_web->update($where, 'in_barang', $data);
        redirect(base_url('admin/barang'));
    }

    public function hapus($id_barang){
        $where = array('id_barang' => $id_barang);
        $cek = $this->m_web->get_id('in_barang', $where)->num_rows();
        if($cek == 0){
            $this->m_web->delete($where,'in_barang');
            redirect(base_url('admin/barang'));
        }else{
            $this->session->set_flashdata('error', 'Data tidak dapat dihapus karena dipakai ditable lain');
            redirect(base_url('admin/barang'));   
        }
    }

}