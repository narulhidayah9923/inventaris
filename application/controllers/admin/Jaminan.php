<?php
class Jaminan extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_web');
	}

	public function index(){
		$data['jaminan'] = $this->m_web->read('in_jaminan')->result();
		$title['title'] = 'Data Jaminan';
		$this->load->view('template/header',$title);
		$this->load->view('admin/v_jaminan', $data);
		$this->load->view('template/footer');
	}

	public function tambah_jaminan(){
		$nama_jaminan = $this->input->post('nama_jaminan');

		$data = array('nama_jaminan' => $nama_jaminan);
		$this->m_web->save('in_jaminan',$data);
		redirect(base_url('admin/jaminan'));
	}

	public function ubah_jaminan(){
        $id_jaminan = $this->input->post('id_jaminan');
        $nama_jaminan = $this->input->post('nama_jaminan');

        $data = array(
            'nama_jaminan' => $nama_jaminan,
        );

        $where = array('id_jaminan' =>$id_jaminan);
        
        $this->m_web->update($where, 'in_jaminan', $data);
        redirect(base_url('admin/jaminan'));
    }

    public function hapus($id_jaminan){
        $where = array('id_jaminan' => $id_jaminan);
        
        $this->m_web->delete($where, 'in_jaminan');
        redirect(base_url('admin/jaminan'));
    }

}