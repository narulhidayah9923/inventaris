<?php
class Jenis extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_web');
    }

    public function index(){
        $data['jenis'] = $this->m_web->read('in_jenis')->result();
        $title['title'] = 'Data Jenis Barang';
        $this->load->view('template/header',$title);
        $this->load->view('admin/v_jenis', $data);
        $this->load->view('template/footer');
    }

    public function tambah_jenis(){
        $kode_jenis = $this->input->post('kode_jenis');
        $nama_jenis = $this->input->post('nama_jenis');
        $keterangan = $this->input->post('keterangan');

        $data = array(
            'kode_jenis' => $kode_jenis,
            'nama_jenis' => $nama_jenis,
            'keterangan' => $keterangan
        );

        $this->m_web->save('in_jenis', $data);
        redirect(base_url('admin/jenis'));
    }

    public function ubah_jenis(){
        $id_jenis = $this->input->post('id_jenis');
        $kode_jenis = $this->input->post('kode_jenis');
        $nama_jenis = $this->input->post('nama_jenis');
        $keterangan = $this->input->post('keterangan');

        $data = array(
            'kode_jenis' => $kode_jenis,
            'nama_jenis' => $nama_jenis,
            'keterangan' => $keterangan
        );

        $where = array('id_jenis' =>$id_jenis);
        
        $this->m_web->update($where, 'in_jenis', $data);
        redirect(base_url('admin/jenis'));
    }
    
    public function hapus($id_jenis){
        $where = array('id_jenis' => $id_jenis);
        
        $this->m_web->delete($where, 'in_jenis');
        redirect(base_url('admin/jenis'));
    }

}