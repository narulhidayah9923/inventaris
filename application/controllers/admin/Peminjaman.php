<?php
class Peminjaman extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_web');
        $this->load->model('m_peminjaman');
    }

    public function index(){
        $data['pesan'] = '';
        $title['title'] = 'Data Peminjaman';
        $data['peminjaman'] = $this->m_peminjaman->read()->result();
        $data['barang'] = $this->m_web->read('in_barang')->result();
        $data['user'] = $this->m_web->read('in_user')->result();
        $data['jaminan'] = $this->m_web->read('in_jaminan')->result();
        $this->load->view('template/header', $title);
        $this->load->view('admin/v_peminjaman',$data);
        $this->load->view('template/footer');
    }

    public function tambah_peminjaman(){
        $title['title'] = 'Data Peminjaman';
        $kode_peminjaman = $this->input->post('kode_peminjaman');
        $tgl_peminjaman = $this->input->post('tgl_peminjaman');
        $id_barang = $this->input->post('id_barang');
        $jumlah_pinjam = $this->input->post('jumlah_pinjam');
        $id_user = $this->input->post('id_user');
        $tgl_kembali_peminjaman = $this->input->post('tgl_kembali_peminjaman');
        $id_jaminan = $this->input->post('id_jaminan');
        $keperluan = $this->input->post('keperluan');

        $where_stock = array('id_barang' => $id_barang);
        $stock = $this->m_web->get_id('in_barang', $where_stock)->row();
        $kurang_stock = $stock->jumlah_barang - $jumlah_pinjam;
        // var_dump($kurang_stock);exit;
        if($kurang_stock >= 0){

            $data = array(
                'kode_peminjaman' => $kode_peminjaman,
                'tgl_peminjaman' => $tgl_peminjaman,
                'id_barang' => $id_barang,
                'jumlah_pinjam' => $jumlah_pinjam,
                'id_user' => $id_user,
                'tgl_kembali_peminjaman' => $tgl_kembali_peminjaman,
                'id_jaminan' => $id_jaminan,
                'keperluan' => $keperluan,
                'status' => 'belum dikembalikan'
            );

            $update_stock = array('jumlah_barang' => $kurang_stock);
            // var_dump($update_stock);exit;
            $this->m_web->update($where_stock, 'in_barang', $update_stock);
            $this->m_web->save('in_peminjaman', $data);
            if($this->session->userdata('level') == 'admin'){
                redirect(base_url('admin/peminjaman'));
            }elseif($this->session->userdata('level') == 'petugas'){
                redirect(base_url('petugas/peminjaman'));
            }else{
                redirect(base_url('peminjam/peminjaman'));
            }
            
        }else{
            $data['pesan'] = "Mohon maaf stock tidak cukup mohon input kembali !!!";
            $data['peminjaman'] = $this->m_peminjaman->read()->result();
            $data['barang'] = $this->m_web->read('in_barang')->result();
            $data['user'] = $this->m_web->read('in_user')->result();
            $data['jaminan'] = $this->m_web->read('in_jaminan')->result();
            $this->load->view('template/header', $title);
            $this->load->view('admin/v_peminjaman',$data);
            $this->load->view('template/footer');
        }
        
    }

    public function kembalikan($id_peminjaman){
        $where = array('id_peminjaman' =>$id_peminjaman , );
        $jumlah_pinjam = $this->m_web->get_id('in_peminjaman', $where)->row();

        $where_barang = array('id_barang' => $jumlah_pinjam->id_barang);
        $stock_sekarang = $this->m_web->get_id('in_barang', $where_barang)->row();

        $update_stock = $stock_sekarang->jumlah_barang + $jumlah_pinjam->jumlah_pinjam;
        $update_stock_ex = array('jumlah_barang' => $update_stock);

        $update_status = array('status' => 'sudah dikembalikan');

        $this->m_web->update($where_barang, 'in_barang', $update_stock_ex);
        $this->m_web->update($where, 'in_peminjaman', $update_status);
        echo "<script>alert('Barang berhasil dikembalikan');</script>";
        if($this->session->userdata('level') == 'admin'){
            // redirect(base_url('admin/peminjaman'));
            // $data['pesan'] = '';
            // $data['peminjaman'] = $this->m_peminjaman->read()->result();
            // $data['barang'] = $this->m_web->read('in_barang')->result();
            // $data['user'] = $this->m_web->read('in_user')->result();
            // $data['jaminan'] = $this->m_web->read('in_jaminan')->result();
            // $this->load->view('template/header');
            // $this->load->view('admin/v_peminjaman',$data);
            // $this->load->view('template/footer');
            echo "<script>location.href = '../../../admin/peminjaman';</script>";
        }elseif($this->session->userdata('level') == 'petugas'){
            redirect(base_url('petugas/peminjaman'));
        }else{
            redirect(base_url('peminjam/peminjaman'));
        }
    }

}