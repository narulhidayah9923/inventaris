<?php
class Ruang extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('m_web');
    }

    public function index(){
        $data['ruang'] = $this->m_web->read('in_ruang')->result();
        $title['title'] = 'Data Ruang';
        $this->load->view('template/header',$title);
        $this->load->view('admin/v_ruang', $data);
        $this->load->view('template/footer');
    }

    public function tambah_ruang(){
        $kode_ruang = $this->input->post('kode_ruang');
        $nama_ruang = $this->input->post('nama_ruang');
        $keterangan = $this->input->post('keterangan');

        $data = array(
            'kode_ruang' => $kode_ruang,
            'nama_ruang' => $nama_ruang,
            'keterangan' => $keterangan
        );

        $this->m_web->save('in_ruang', $data);
        redirect(base_url('admin/ruang'));
    }

    public function ubah_ruang(){
        $id_ruang = $this->input->post('id_ruang');
        $kode_ruang = $this->input->post('kode_ruang');
        $nama_ruang = $this->input->post('nama_ruang');
        $keterangan = $this->input->post('keterangan');

        $data = array(
            'kode_ruang' => $kode_ruang,
            'nama_ruang' => $nama_ruang,
            'keterangan' => $keterangan
        );

        $where = array('id_ruang' =>$id_ruang);
        
        $this->m_web->update($where, 'in_ruang', $data);
        redirect(base_url('admin/ruang'));
    }
    
    public function hapus($id_ruang){
        $where = array('id_ruang' => $id_ruang);
        
        $this->m_web->delete($where, 'in_ruang');
        redirect(base_url('admin/ruang'));
    }

}