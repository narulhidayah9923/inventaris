<?php
class Dashboard extends CI_Controller{

    function __construct(){
        parent::__construct();
    }

    public function index(){
    	$title['title'] = 'Dashboard';
        $this->load->view('template/header', $title);
        $this->load->view('admin/v_index');
        $this->load->view('template/footer');
    }

}