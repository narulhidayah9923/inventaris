<?php
class Home extends CI_Controller{

    function __construct(){
        parent::__construct();
        // if($this->session->userdata('status') != 'login'){
        //     redirect(base_url('login'));
        // }
    }

    public function index(){
        $this->load->view('template/header');
        $this->load->view('admin/v_index');
        $this->load->view('template/footer');
    }

}