-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Feb 2019 pada 08.55
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_barang`
--

CREATE TABLE `in_barang` (
  `id_barang` int(15) NOT NULL,
  `kode_barang` varchar(15) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah_barang` int(10) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `id_ruang` int(15) NOT NULL,
  `id_user` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_barang`
--

INSERT INTO `in_barang` (`id_barang`, `kode_barang`, `nama_barang`, `keterangan`, `jumlah_barang`, `id_jenis`, `id_ruang`, `id_user`) VALUES
(3, 'KB0602109727', 'ASUS A3 ', 'dsadasdasd', 17, 6, 22, NULL),
(5, 'KB0602535827', '#SayangUangNya', 'buku baru', 17, 1, 22, NULL),
(6, 'KB0502184071', 'Printer', 'printer', 10, 6, 21, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_jaminan`
--

CREATE TABLE `in_jaminan` (
  `id_jaminan` int(15) NOT NULL,
  `nama_jaminan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_jaminan`
--

INSERT INTO `in_jaminan` (`id_jaminan`, `nama_jaminan`) VALUES
(1, 'KTP'),
(2, 'Kartu Pelajar'),
(3, 'SIM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_jenis`
--

CREATE TABLE `in_jenis` (
  `id_jenis` int(15) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_jenis`
--

INSERT INTO `in_jenis` (`id_jenis`, `kode_jenis`, `nama_jenis`, `keterangan`) VALUES
(1, 'KJ1002241598', 'Buku', 'buku baru'),
(3, 'KJ0602427997', 'Laptop', 'test'),
(5, 'KJ0502588946', 'asd', 'asdasda'),
(6, 'KJ0502021863', 'Elektronik', 'alat elektronik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_level`
--

CREATE TABLE `in_level` (
  `id_level` int(15) NOT NULL,
  `nama_level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_level`
--

INSERT INTO `in_level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'pegawai'),
(3, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_peminjaman`
--

CREATE TABLE `in_peminjaman` (
  `id_peminjaman` int(15) NOT NULL,
  `kode_peminjaman` varchar(10) NOT NULL,
  `tgl_peminjaman` date NOT NULL,
  `id_barang` int(15) NOT NULL,
  `jumlah_pinjam` int(5) NOT NULL,
  `id_user` int(15) NOT NULL,
  `tgl_kembali_peminjaman` date NOT NULL,
  `id_jaminan` int(15) NOT NULL,
  `keperluan` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_peminjaman`
--

INSERT INTO `in_peminjaman` (`id_peminjaman`, `kode_peminjaman`, `tgl_peminjaman`, `id_barang`, `jumlah_pinjam`, `id_user`, `tgl_kembali_peminjaman`, `id_jaminan`, `keperluan`, `status`) VALUES
(7, 'KP09025451', '2019-02-20', 5, 1, 1, '2019-02-22', 1, '2adsa', 'sudah dikembalikan'),
(8, 'KP05020736', '2019-02-22', 6, 1, 1, '2019-02-23', 1, 'da', 'sudah dikembalikan'),
(9, 'KP05025164', '2019-02-22', 6, 2, 1, '2019-02-22', 1, '', 'sudah dikembalikan'),
(10, 'KP05023166', '2019-02-21', 3, 5, 1, '2019-02-22', 1, '123', 'sudah dikembalikan'),
(11, 'KP07022052', '2019-02-21', 3, 10, 1, '2019-02-28', 2, 'asdas', 'sudah dikembalikan'),
(12, 'KP07024997', '2019-02-21', 3, 1, 1, '2019-02-22', 1, '123asd', 'sudah dikembalikan'),
(13, 'KP07022791', '2019-02-21', 5, 1, 2, '2019-02-22', 3, '1123', 'sudah dikembalikan'),
(14, 'KP07020522', '2019-02-20', 5, 1, 3, '2019-02-22', 2, '1asd', 'sudah dikembalikan'),
(15, 'KP07021349', '2019-02-21', 5, 1, 3, '2019-02-22', 2, '123asd', 'sudah dikembalikan'),
(16, 'KP07025718', '2019-02-21', 6, 1, 1, '2019-02-25', 2, 'ujikom', 'sudah dikembalikan'),
(17, 'KP07022132', '2019-02-21', 3, 1, 1, '2019-02-22', 2, '1123', 'sudah dikembalikan'),
(18, 'KP08022971', '2019-02-19', 3, 1, 1, '2019-02-21', 1, '1sdadas', 'sudah dikembalikan'),
(19, 'KP08022656', '2019-02-21', 5, 2, 1, '2019-02-21', 1, 'adsa', 'sudah dikembalikan'),
(20, 'KP08022623', '2019-02-21', 3, 1, 1, '2019-02-22', 2, '1sdadas', 'sudah dikembalikan'),
(21, 'KP08021528', '2019-02-21', 3, 2, 2, '2019-02-23', 3, 'sad', 'sudah dikembalikan'),
(22, 'KP08022087', '2019-02-21', 5, 1, 1, '2019-02-22', 1, 'sad', 'sudah dikembalikan'),
(23, 'KP08025585', '0000-00-00', 3, 2, 1, '2019-02-22', 2, 'ads', 'sudah dikembalikan'),
(24, 'KP08021032', '0000-00-00', 5, 6, 2, '0000-00-00', 2, '1sdadas', 'sudah dikembalikan'),
(25, 'KP09025628', '2019-02-15', 3, 0, 2, '2019-02-12', 1, 'umu', 'belum dikembalikan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_ruang`
--

CREATE TABLE `in_ruang` (
  `id_ruang` int(15) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_ruang`
--

INSERT INTO `in_ruang` (`id_ruang`, `kode_ruang`, `nama_ruang`, `keterangan`) VALUES
(21, 'KR1002177627', 'Kepala Sekolah', 'ruang kepsek'),
(22, 'KR1002345411', 'Lab. Fotografi', 'laboratorium'),
(23, 'KR0602315985', 'Ruang Guru', 'asjlk');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in_user`
--

CREATE TABLE `in_user` (
  `id_user` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `id_level` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `in_user`
--

INSERT INTO `in_user` (`id_user`, `username`, `password`, `nama_user`, `id_level`) VALUES
(1, 'narul9923', '708711321193ec43839678a17ce6ac97', 'Narul Hidayah', 1),
(2, 'narul123', '8e5517f8631dd8f57e9d0bd2dad7531b', 'narullll', 2),
(3, 'narul', '8c5795fa3d97a4a75adc4c145951bc36', 'narul', 3),
(4, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user baru', 3),
(10, 'adsadasasd', '22355cc09048513ad339c18a323f69c8', 'lkasjd', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `in_barang`
--
ALTER TABLE `in_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `in_jaminan`
--
ALTER TABLE `in_jaminan`
  ADD PRIMARY KEY (`id_jaminan`);

--
-- Indeks untuk tabel `in_jenis`
--
ALTER TABLE `in_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `in_level`
--
ALTER TABLE `in_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `in_peminjaman`
--
ALTER TABLE `in_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indeks untuk tabel `in_ruang`
--
ALTER TABLE `in_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indeks untuk tabel `in_user`
--
ALTER TABLE `in_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `in_barang`
--
ALTER TABLE `in_barang`
  MODIFY `id_barang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `in_jaminan`
--
ALTER TABLE `in_jaminan`
  MODIFY `id_jaminan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `in_jenis`
--
ALTER TABLE `in_jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `in_level`
--
ALTER TABLE `in_level`
  MODIFY `id_level` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `in_peminjaman`
--
ALTER TABLE `in_peminjaman`
  MODIFY `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `in_ruang`
--
ALTER TABLE `in_ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `in_user`
--
ALTER TABLE `in_user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
